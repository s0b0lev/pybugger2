from distutils.core import setup

setup(
    name='pybugger2',
    version='0.6',
    packages=['pybugger2'],
    package_data={'pybugger2': ['*.txt']},
    url='https://github.com/fareskalaboud/pybugger2',
    license='GNU General Public License v3.0',
    author='fareskalaboud',
    author_email='fares.alaboud@gmail.com',
    description='Pybugger is an easy to use color-supported CLI debugging tool for python.'
)
